const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    if (
      req &&
      req.body &&
      req.body.name &&
      uniqueName({ name: req.body.name.toLowerCase() }) &&
      uniqueName({ name: req.body.name.toUpperCase() }) &&
      req.body.health &&
      req.body.health <= 100 &&
      req.body.power &&
      req.body.power < 100 &&
      req.body.defense &&
      req.body.defense <= 10
    ){
      next();
    }else if(
      req &&
      req.body &&
      !req.body.name
    ){
      res.status(400).json( { error: true, message: "Fighter's name not be empty"} )
    }else if(
      req &&
      req.body &&
      !uniqueName( { name: req.body.name }) &&
      !uniqueName({ name: req.body.name.toUpperCase() })
    ){
      res.status(400).json( { error: true, message: "A fighter with this name already exists."} )
    }else if(
      req &&
      req.body &&
      req.body.health && 
      req.body.health > 100
    ){
      res.status(400).json( { error: true, message: "Fighter's health not be more than 100"} )
    }else if(
      req &&
      req.body &&
      req.body.power && 
      req.body.power >= 100
    ){
      res.status(400).json( { error: true, message: "Fighter's power not be more than 10"} )
    }else if(
      req &&
      req.body &&
      req.body.defense && 
      req.body.defense > 10
    ){
      res.status(400).json( { error: true, message: "Fighter's defense not be more than 10"} )
    }else{
      res.status(400).json( { error: true, message: 'All input fields must be write in'} )
    }
    
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    if(!FighterService.searchFighter({ id: req.params.id })){
      res.status(400).json({ error: true, message: 'User not found'});
    }else{
      let updateData = {};
      for(let key in req.body) {
        if (Object.keys(fighter).includes(key)) {
          updateData[key] = req.body[key];         
        }
      }

      if(updateData.name && !uniqueName( { name: updateData.name })){
        res.status(400).json( { error: true, message: "A fighter with this name already exists."} )
      }else if(updateData.health && updateData.health > 100){
        res.status(400).json( { error: true, message: "Fighter's health not be more than 100"} )
      }else if(updateData.power && updateData.power >= 100){
        res.status(400).json( { error: true, message: "Fighter's power not be more than 100"} )
      }else if(updateData.defense && updateData.defense > 10){
        res.status(400).json( { error: true, message: "Fighter's defense not be more than 10"} )
      }else{
        next();
      }
    }
}

function uniqueName(name){
  return FighterService.searchFighter(name) ? false : true;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;