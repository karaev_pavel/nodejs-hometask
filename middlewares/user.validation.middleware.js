const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    if(!checkData(req.body, user)){
      res.status(400).json( { error: true, message: 'Object do not equal to user models object'} )
    }else if (
      req &&
      req.body &&
      req.body.firstName &&
      req.body.lastName &&
      req.body.email &&
      /@gmail.com$/.test(req.body.email) &&
      uniqueValue({email: req.body.email}) &&
      req.body.phoneNumber &&
      /^\+380\d{9}$/.test(req.body.phoneNumber) &&
      uniqueValue({phoneNumber : req.body.phoneNumber}) &&
      req.body.password
    ) {
      next();
    } else if(
      req &&
      req.body &&
      !req.body.firstName
    ) {
      res.status(400).json( { error: true, message: 'First Name not be empty'} )
    } else if(
      req &&
      req.body &&
      !req.body.lastName
    ) {
      res.status(400).json( { error: true, message: 'Last Name not be empty'} )
    } else if(
      req &&
      req.body &&
      req.body.email &&
      !/@gmail.com$/.test(req.body.email)
    ) {
      res.status(400).json( { error: true, message: 'Email should end @gmail.com.' } )
    } else if(
      req &&
      req.body &&
      req.body.email &&
      !uniqueValue({ email : req.body.email })
    ) {
      res.status(400).json( { error: true, message: 'A user with this email already exists.' } )
    } else if(
      req &&
      req.body &&
      req.body.phoneNumber &&
      !/^\+380\d{9}$/.test(req.body.phoneNumber)
    ) {
      res.status(400).json( { error: true, message: 'Phone number should start +380... .' } )
    } else if(
      req &&
      req.body &&
      req.body.phoneNumber &&
      !uniqueValue({ phoneNumber : req.body.phoneNumber })
    ) {
      res.status(400).json( { error: true, message: 'A user with this phone number already exists.' } )
    } else if(
      req &&
      req.body &&
      req.body.password &&
      req.body.password.length < 3
    ) {
      res.status(400).json( { error: true, message: 'Password must be more than 3 symbols'} )
    }else{
      res.status(400).json( { error: true, message: 'All input fields must be write in'} )
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    if(!UserService.search({ id: req.params.id })){
      res.status(400).json({ error: true, message: 'User not found'});
    }else{
      let updateData = {};
      for(let key in req.body) {
        if (Object.keys(user).includes(key)) {
          updateData[key] = req.body[key];         
        }
      }
     
      if(updateData.email && !/@gmail.com$/.test(updateData.email)){
        res.status(400).json( { error: true, message: 'Email should end @gmail.com.' }); 
      }else if(updateData.email && !uniqueValue({ email: updateData.email })){
        res.status(400).json( { error: true, message: 'A user with this email already exists.' } );
      }else if(updateData.phoneNumber && !/^\+380\d{9}$/.test(updateData.phoneNumber)){
        res.status(400).json( { error: true, message: 'Phone number should start +380... .' } )
      }else if(updateData.phoneNumber && !uniqueValue({ phoneNumber: updateData.phoneNumber })){
        res.status(400).json( { error: true, message: 'A user with this phone number already exists.' } )
      }else if(updateData.password && updateData.password.length < 3) {
       res.status(400).json( { error: true, message: 'Password must be more than 3 symbols'} )
      }else{
        next();
      }
      
    }
}

function uniqueValue(val){
  return UserService.search(val) ? false : true;
}

function checkData(data, user){
  const arrUserValue = Object.keys(user);
  arrUserValue.shift();
  return JSON.stringify(arrUserValue.sort()) === JSON.stringify(Object.keys(data).sort());
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;