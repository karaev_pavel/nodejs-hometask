const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    create(data){
      const user = UserRepository.create(data);
      if(!user) {
        return null;
      }
      return user;
    }

    getAllUsers(){
      return UserRepository.getAll();
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    updateUser(id, updateDate){
      const result = UserRepository.update(id, updateDate);

      return result;
    }

    deleteUser(id){
      const result = UserRepository.delete(id);

      return result;
    }
}

module.exports = new UserService();