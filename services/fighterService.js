const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  // TODO: Implement methods to work with fighters
  getFighters(){
    return FighterRepository.getAll();
  }

  searchFighter(fighterId){
    const fighter = FighterRepository.getOne(fighterId);

    if(!fighter) {
      return null;
    }
    return fighter;
  }

  createFighter(data){
    const fighter = FighterRepository.create(data);

    if(!fighter) {
      return null;
    }
    return fighter;
  }

  updateFighter(fighterId, updateDate){
    const fighter = FighterRepository.update(fighterId, updateDate);

    return fighter;
  }

  deleteFighter(fighterId){
    const fighter = FighterRepository.delete(fighterId);

    return fighter;
  }
}

module.exports = new FighterService();