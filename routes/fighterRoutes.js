const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function(req, res){
  const fighters = FighterService.getFighters();

  res.status(200).json(fighters);
})

router.get('/:id', function(req, res){
  const fighter = FighterService.searchFighter( { id: req.params.id } );

  res.status(200).json(fighter);
})

router.post('/', createFighterValid, function(req, res){
  const fighter = FighterService.createFighter(req.body);

  res.status(200).json(fighter);
})

router.put('/:id', updateFighterValid, function(req, res){
  const fighter = FighterService.updateFighter(req.params.id, req.body);

  res.status(200).json(fighter);
})

router.delete('/:id', function(req, res){
  const fighter = FighterService.deleteFighter(req.params.id);

  if(!fighter.length){
    res.status(404).json( { error: true, message: 'Fighter not found'} );
  }else if(fighter){
    res.status(200).json(fighter);
  }
})

module.exports = router;