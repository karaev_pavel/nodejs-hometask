const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', function(req, res, next) {
  const users = UserService.getAllUsers();

  res.status(200).json(users);
});

router.get('/:id', function(req, res, next) {
  const user = UserService.search({ id: req.params.id });
  
  res.status(200).json(user);
});

router.post('/', createUserValid, function(req, res, next) {
  const user = UserService.create(req.body);

  res.status(200).json(user);
});

router.put('/:id', updateUserValid, function(req, res, next) {
  const user = UserService.updateUser(req.params.id, req.body);

  res.status(200).json(user);
});

router.delete('/:id', function(req, res, next) {
  const user = UserService.deleteUser(req.params.id);

  if(!user.length){
    res.status(404).json( { error: true, message: 'User not found'} );
  }else if(user){
    res.status(200).json(user);
  }
});

module.exports = router;